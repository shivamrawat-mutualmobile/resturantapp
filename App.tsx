import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import HomeScreen from './src/Screens/Homescreen';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import ResultScreen from './src/Screens/ResultScreen';
import {StackParamList} from './src/Interface/Interfaces'

const Stack  =createStackNavigator<StackParamList>();

const App = () => {
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen}/>
        <Stack.Screen name="Result"  options={{title:"ResultScreen"}}  component={ResultScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
