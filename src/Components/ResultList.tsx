/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
    FlatList,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    useColorScheme,
    View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ResultComponent from './ResultComponent';
import type { Result } from '../Interface/Interfaces'
import {  StackNavigationProp } from '@react-navigation/stack'
import {StackParamList} from '../Interface/Interfaces'
interface Props {
    title: string,
    result: Result[]
}

const ResultList: React.FC<Props> = ({ title, result }) => {
    if (result.length == 0) {
        return null;
    }
    
    const navigation = useNavigation<StackNavigationProp<StackParamList>>();
    return (
        <View style={styles.container}>
            <Text style={styles.header}>{title}</Text>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={result}
                keyExtractor={result => result.id}
                renderItem={({ item }) => {
                    return <TouchableOpacity onPress={() => {
                        navigation.navigate("Result", { id: item.id });
                    }}>
                        <ResultComponent result={item} />
                    </TouchableOpacity>
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    },
    header: {
        fontSize: 22,
        fontWeight: 'bold',
        margin: 20
    }
});

export default ResultList;
