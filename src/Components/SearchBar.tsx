import React, { FC } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

interface props {
  term: string,
  onChangeTerm: (term: string) => void,
  onSubmitTerm: (term: string) => void
}

const SearchBar: FC<props> = ({ term, onChangeTerm, onSubmitTerm }) => {
  return (
    <View style={styles.container}>
      <Icon name="search" color="#000" size={30} style={styles.icon} />
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        placeholder="Search"
        style={styles.input}
        value={term}
        onChangeText={onChangeTerm}
        onEndEditing={() => onSubmitTerm(term)}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ccc",
    height: 60,
    margin: 20,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
    flex: 1,
    fontSize: 18
  },
  icon: {
    fontSize: 30,
    padding: 10
  },
});

export default SearchBar;
