import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Result } from '../Interface/Interfaces';



interface Props {
  result: Result
}
const ResultComponent: React.FC<Props> = ({ result }) => {
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: result.image_url }}
        style={styles.image}
      />
      <Text>{result.name}</Text>
      <Text>{result.rating} stars, {result.review_count} reviews</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  icon: {
    fontSize: 30,
    padding: 10
  },
  input: {
    flex: 1
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4,
    marginBottom: 5
  }
});

export default ResultComponent;
