
export interface Result{
    id : string 
    price : string,
    image_url:string , 
     rating: number,
     review_count :number ,
     name: string ,
     photos:string[]
}
export interface ResultDetail{
    photos:string[]
}

export type StackParamList = {
    Home: {};
    Result: { id: string };
};