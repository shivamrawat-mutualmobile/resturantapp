import React, { useEffect, useState } from 'react';
import yelp from '../Api/yelp';
import { ResultDetail } from '../Interface/Interfaces';
export default (route:{params: {id:string}}):[ResultDetail,string]=>{
    const [result, setResult] = useState({photos:[]});
    const [error, setError] = useState("");
    
    useEffect(() => {
        const id =route.params.id;
        yelp.get(`${id}`)
            .then(data => {
                console.log(data.data);
                setResult(data.data);
            })
            .catch(err=>{
                setError(err);
            })
    }, [])
    return [result, error]
}