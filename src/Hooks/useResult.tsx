import React, { useState ,useEffect} from 'react';
import yelp from '../Api/yelp';
import {Result} from '../Interface/Interfaces'

export default ():[Array<Result> ,(term:string)=>void, string]=>{

    const [result,setResult]= useState([]); 
    const [error,setError]= useState(""); 
    
    useEffect(()=>{
        onTermSubmit("pizza");
    },[])
    function onTermSubmit(term:string){
        console.log("here")
        yelp.get(`/search`,{
            params: {
                limit: 50,
                term: term,
                location: 'san jose'
              }
        })
        .then(data=>{
            setResult(data.data.businesses);
        })
        .catch(err=>{
            setError(err);
        })
    }
    
    return [result,onTermSubmit,error]; 
}