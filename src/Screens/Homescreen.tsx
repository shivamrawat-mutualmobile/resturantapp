import React from 'react';
import { useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import SearchBar from '../Components/SearchBar';
import ResultList from '../Components/ResultList';
import useResult from '../Hooks/useResult';

const HomeScreen = () => {
  const [term,setTerm]=useState<string>("");
  const [result,onTermSubmit, error]= useResult();
  function filterPrice(value:string){
    return result.filter(data=>{
      return data.price === value;
    })
  }
  return(
    <>
      <SearchBar term={term} onChangeTerm={setTerm} onSubmitTerm={onTermSubmit}/>
      <ScrollView>
                <ResultList title="Cheap" result={filterPrice("$")}/>
                <ResultList title="Expensive"  result={filterPrice("$$")}/>
                <ResultList title="Big Spender"  result={filterPrice("$$$")}/>
            </ScrollView>
    </>
  )
};

const styles = StyleSheet.create({
  
});

export default HomeScreen;
