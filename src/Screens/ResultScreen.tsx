/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React, { useEffect, useState } from 'react';
 import {
     FlatList,
     Image,
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,Dimensions,
     View,
 } from 'react-native';
 import yelp from '../api/yelp';
 import useDetails from '../Hooks/useDetails';
 
 interface Props{
     route: any,
     navigation : any 
 }
 
 const ResultScreen:React.FC<Props> = ({ route,navigation }) => {
     const dimensions = Dimensions.get('window');
     const imageHeight = Math.round(dimensions.width * 9 / 16);
     const imageWidth = dimensions.width;
     const [result,error] = useDetails(route);
     return (
         <>
         <View style={{flex:1,justifyContent:'space-around',height:imageHeight}}>
         <FlatList
         data={result.photos}
         renderItem={({item})=>{
             return <Image source={{uri:item}} style={[styles.image,{width:imageWidth,height:imageHeight}]}/>
         }}
         />
         </View>
         </>
     );
 };
 
 const styles = StyleSheet.create({
     image:{
         flex:1
     }
 });
 
 export default ResultScreen;
 
 